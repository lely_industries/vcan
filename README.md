# Virtual CAN interface (vcan) emulator

The `libvcan` library and `vcan-exec` command-line tool allow a user to run a
command with one or more emulated virtual CAN interfaces. This behavior extends
to any child processes spawned by the invoked command.

The emulator provides a socket interface similar to SocketCAN and is intended
for environments in which the `vcan.ko` kernel module is not available, such as
WSL2, or in which network interfaces cannot be created, such as unprivileged
Docker containers.

The emulator works by using the `LD_PRELOAD` mechanism to intercept system calls
and converts `AF_CAN` sockets to `AF_INET6` sockets, which forward CAN frames
over UDP to IPv4 or IPv6 multicast groups.

## Issues

- Does not work on statically linked or setuid binaries.
- `vcan-exec` is thread-safe. Unfortunately, this breaks the async-signal-safety
  guarantee of `bind()`, `close()`, `getpeername()`, `getsockname()`,
  `getsockopt()`, `read()`, `recv()`, `recvfrom()`, `recvmsg()`, `send()`,
  `sendmsg()`, `sendto()`, `socket()` and `write()`.
- Only the RAW SocketCAN protocol is supported, not Broadcast Manager (BCM) or
  J1939.
- Due to user space filtering, read operations may block even when `select()`,
  `poll()` or `epoll_wait()` report the socket to be ready for reading. It is
  recommended to always open sockets in non-blocking mode when using this
  emulator.
- Emulated `AF_CAN` sockets that remain open across an `execve()` become regular
  `AF_INET6` sockets.
- Only a small subset of system calls used to query network interfaces is
  emulated:
  - `if_indextoname()` and `if_nametoindex()`,
  - the `SIOCGIFNAME`, `SIOCGIFINDEX`, `SIOCSIFFLAGS`, `SIOCGIFMTU`,
  - `SIOCGIFHWADDR`, `SIOCGIFMAP` and `SIOCGIFTXQLEN` `ioctl()` requests,
  - and the `RTM_GETLINK` `rtnetlink` request, provided the request queries
    emulated interfaces explicitly by name, index, address family (`AF_CAN`) or
    device type ("vcan").

## Installation

The virtual CAN interface emulator uses the Autotools build system. To build,
run:

    $ autoreconf -i
    $ ./configure
    $ make

To install, run:

    $ make install

## Documentation

See the [vcan-exec](doc/vcan-exec.md.in) man page for documentation.

## Licensing

Copyright 2022 [Lely Industries N.V.](http://www.lely.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
