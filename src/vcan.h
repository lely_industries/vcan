// SPDX-License-Identifier: Apache-2.0
#ifndef VCAN_VCAN_H
#define VCAN_VCAN_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef LIBVCAN
#define LIBVCAN "libvcan.so"
#endif

#ifndef VCAN_IFINDEX
#define VCAN_IFINDEX 1000
#endif

#ifndef VCAN_IFADDR
#define VCAN_IFADDR "vcan0=ff02::1"
#endif

#ifndef VCAN_PORT
#define VCAN_PORT 8884
#endif

#endif // !VCAN_VCAN_H
