// SPDX-License-Identifier: Apache-2.0

#include "vcan.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define STRINGIFY(s) STRINGIFY_(s)
#define STRINGIFY_(s) #s

#define CMD "vcan-exec"

// clang-format off
#define HELP \
	"Usage: " CMD " [OPTION]... [-] COMMAND [ARGUMENT]... \n" \
	"Run COMMAND with one or more virtual CAN interfaces. CAN frames sent to a\n" \
	"virtual CAN interface are forwarded over UDP to an IPv4 or IPv6 multicast\n" \
	"address.\n\n" \
	"Options:\n" \
	"  -h, --help        show this information\n" \
	"  -i, --index=INDEX the index of the first virtual CAN interface (default: " STRINGIFY(VCAN_IFINDEX) ")\n" \
	"  -a, --address=NAME=ADDRESS\n" \
	"                    map the virtual CAN interface NAME to multicast address\n" \
	"                    ADDRESS (default: " VCAN_IFADDR ")\n" \
	"  -p, --port=PORT   send/receive CAN frames on port PORT (default: " STRINGIFY(VCAN_PORT) ")\n" \
	"  -t, --trace=WHICH print trace information for the specified function calls\n" \
	"    WHICH is one of the following:\n" \
	"      all           all intercepted function calls\n" \
	"      can           only those function calls that involve a virtual CAN interface\n"

// clang-format on

static int env_append_vcan_ifaddr(const char *string);
static int env_prepend_ld_preload(const char *path);

int
main(int argc, char *argv[])
{
	int errflg = 0;
	opterr = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-')
			break;
		if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "help")) {
				printf("%s\n", HELP);
				return EXIT_SUCCESS;
			} else if (!strncmp(arg, "index=", 6)) {
				setenv("VCAN_IFINDEX", arg + 6, 1);
			} else if (!strncmp(arg, "address=", 8)) {
				if (env_append_vcan_ifaddr(optarg + 8) == -1) {
					perror(CMD ": unable to set VCAN_IFADDR");
					return EXIT_FAILURE;
				}
			} else if (!strncmp(arg, "port=", 5)) {
				setenv("VCAN_PORT", arg + 5, 1);
			} else if (!strncmp(arg, "trace=", 6)) {
				setenv("VCAN_TRACE", arg + 6, 1);
			} else {
				fprintf(stderr, CMD ": illegal option -- %s\n",
						arg);
				errflg = 1;
			}
		} else {
			int c = getopt(argc, argv, "hi:a:p:t:");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr,
						CMD
						": option requires an argument -- %c\n",
						optopt);
				errflg = 1;
				break;
			case '?':
				fprintf(stderr, CMD ": illegal option -- %c\n",
						optopt);
				errflg = 1;
				break;
			case 'h': puts(HELP); return EXIT_SUCCESS;
			case 'i': setenv("VCAN_IFINDEX", optarg, 1); break;
			case 'a':
				if (env_append_vcan_ifaddr(optarg) == -1) {
					perror(CMD ": unable to set VCAN_IFADDR");
					return EXIT_FAILURE;
				}
				break;
			case 'p': setenv("VCAN_PORT", optarg, 1); break;
			case 't': setenv("VCAN_TRACE", optarg, 1); break;
			}
		}
	}

	if (optind >= argc) {
		fprintf(stderr, CMD ": no command specified\n");
		errflg = 1;
	}

	if (errflg) {
		fputs(HELP, stderr);
		return EXIT_FAILURE;
	}

	if (env_prepend_ld_preload(LIBVCAN) == -1) {
		perror(CMD ": unable to set LD_PRELOAD");
		return EXIT_FAILURE;
	}

	execvp(argv[optind], &argv[optind]);
	perror(argv[optind]);

	return EXIT_FAILURE;
}

static int
env_append_vcan_ifaddr(const char *string)
{
	char *envval = getenv("VCAN_IFADDR");
	if (envval) {
		int n = snprintf(NULL, 0, "%s,%s", envval, string);
		if (n < 0)
			return -1;
		char buf[n + 1];
		sprintf(buf, "%s,%s", envval, string);
		if (setenv("VCAN_IFADDR", buf, 1) == -1)
			return -1;
	} else {
		setenv("VCAN_IFADDR", string, 0);
	}
	return 0;
}

static int
env_prepend_ld_preload(const char *path)
{
	char *envval = getenv("LD_PRELOAD");
	if (envval) {
		int n = snprintf(NULL, 0, "%s:%s", path, envval);
		if (n < 0)
			return -1;
		char buf[n + 1];
		sprintf(buf, "%s:%s", path, envval);
		if (setenv("LD_PRELOAD", buf, 1) == -1)
			return -1;
	} else {
		setenv("LD_PRELOAD", path, 0);
	}
	return 0;
}
