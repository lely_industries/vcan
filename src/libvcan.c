// SPDX-License-Identifier: Apache-2.0

#include "vcan.h"

#include <arpa/inet.h>
#include <assert.h>
#include <dlfcn.h>
#include <errno.h>
#include <limits.h>
#include <net/if.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <time.h>
#include <unistd.h>

#include <linux/can/raw.h>
#include <linux/if_arp.h>
#include <linux/rtnetlink.h>
// Ignore variadic ioctl() definition.
#define ioctl hide_ioctl
#include <sys/ioctl.h>
#undef ioctl

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) < (b) ? (b) : (a))

#define SOCK_TYPE_MASK 0xf

#define CMSG_MAXLEN(mhdr, cmsg) \
	((size_t)((char *)(mhdr)->msg_control + (mhdr)->msg_controllen \
			- (char *)(cmsg)))

#define CMSG_SPACE_PKTINFO \
	CMSG_SPACE(MAX(sizeof(struct in_pktinfo), sizeof(struct in6_pktinfo)))

#define NLHMSG_BUFLEN 1024

#define VCAN_MTU (4 + 4 + 4 + 4 + 1 + 1 + CAN_MAX_DLEN)
#define VCANFD_MTU (4 + 4 + 4 + 4 + 1 + 1 + CANFD_MAX_DLEN)

static void vcan_init_routine(void);

static int vcan_init_errsv;

static int vcan_init(void) __attribute__((constructor));
static void vcan_fini(void) __attribute__((destructor));

static long vcan_hostid;
static pid_t vcan_pid;

#define VCAN_TRACE_ALL() VCAN_TRACE(TRACE_ALL)

#define VCAN_TRACE_CAN() VCAN_TRACE(TRACE_CAN)

#define VCAN_TRACE(trace) \
	do { \
		if (vcan_trace == (trace)) \
			vcan_print_trace(__func__); \
	} while (0)

static void vcan_print_trace(const char *func);

enum { TRACE_NONE, TRACE_ALL, TRACE_CAN };
static int vcan_trace = TRACE_NONE;

static unsigned short vcan_port;
static unsigned int vcan_ifindex;

struct vcan_ifaddr {
	unsigned int if_index;
	char if_name[IF_NAMESIZE];
	union {
		struct sockaddr addr;
		struct sockaddr_in sin;
		struct sockaddr_in6 sin6;
	};
	socklen_t addrlen;
	_Alignas(NLMSG_ALIGNTO) char nlh[NLHMSG_BUFLEN];
};

#define VCAN_IFADDR_INIT \
	{ \
		0, { 0 }, { { .sa_family = AF_UNSPEC } }, \
				sizeof(struct sockaddr), \
		{ \
			0 \
		} \
	}

static struct vcan_ifaddr *vcan_ifaddr;
static size_t vcan_ifaddr_size;

static int vcan_ifaddr_insert(const char *begin, const char *end);

static const struct vcan_ifaddr *vcan_ifaddr_find_by_index(
		unsigned int if_index);
static const struct vcan_ifaddr *vcan_ifaddr_find_by_name(const char *if_name);
static const struct vcan_ifaddr *vcan_ifaddr_find_by_addr(
		const struct sockaddr *addr, socklen_t addrlen);
static const struct vcan_ifaddr *vcan_ifaddr_find_by_cmsg(
		const struct cmsghdr *cmsg);

// From <net/if.h>:
static unsigned int (*next_if_nametoindex)(const char *ifname);
static char *(*next_if_indextoname)(unsigned int ifindex, char *ifname);

// From <sys/socket.h>:
static int (*next_bind)(
		int sockfd, const struct sockaddr *addr, socklen_t addrlen);
static int (*next_getpeername)(
		int sockfd, struct sockaddr *addr, socklen_t *addrlen);
static int (*next_getsockname)(
		int sockfd, struct sockaddr *addr, socklen_t *addrlen);
static int (*next_getsockopt)(int sockfd, int level, int optname, void *optval,
		socklen_t *optlen);
static ssize_t (*next_recv)(int sockfd, void *buf, size_t len, int flags);
static ssize_t (*next_recvfrom)(int sockfd, void *buf, size_t len, int flags,
		struct sockaddr *src_addr, socklen_t *addrlen);
static int (*next_recvmmsg)(int sockfd, struct mmsghdr *msgvec,
		unsigned int vlen, int flags, struct timespec *timeout);
static ssize_t (*next_recvmsg)(int sockfd, struct msghdr *msg, int flags);
static ssize_t (*next_send)(int sockfd, const void *buf, size_t len, int flags);
static int (*next_sendmmsg)(int sockfd, struct mmsghdr *msgvec,
		unsigned int vlen, int flags);
static ssize_t (*next_sendmsg)(int sockfd, const struct msghdr *msg, int flags);
static ssize_t (*next_sendto)(int sockfd, const void *buf, size_t len,
		int flags, const struct sockaddr *dest_addr, socklen_t addrlen);
static int (*next_setsockopt)(int sockfd, int level, int optname,
		const void *optval, socklen_t optlen);
static int (*next_socket)(int domain, int type, int protocol);

// From <sys/uio.h>:
static ssize_t (*next_readv)(int fd, const struct iovec *iov, int iovcnt);
static ssize_t (*next_writev)(int fd, const struct iovec *iov, int iovcnt);

// From <unistd.h>
static int (*next_close)(int fd);
static ssize_t (*next_read)(int fd, void *buf, size_t count);
static ssize_t (*next_write)(int fd, const void *buf, size_t count);

// From <sys/ioctl.h>
static int (*next_ioctl)(int fd, unsigned long request, void *arg);

static pthread_mutex_t vcan_mutex = PTHREAD_MUTEX_INITIALIZER;

static inline void vcan_lock(void);
static inline void vcan_unlock(void);

struct vcan_sock {
	int fd;
	const struct vcan_ifaddr *ifaddr;
	int recv_own_msgs;
	int fd_frames;
	int join_filters;
	int count;
	struct can_filter dfilter;
	struct can_filter *filter;
	can_err_mask_t err_mask;
};

#define VCAN_SOCK_INIT(sock) \
	{ \
		-1, NULL, 0, 0, 0, 1, { 0, 0 }, &(sock)->dfilter, 0 \
	}

static struct vcan_sock *vcan_sock;
static size_t vcan_sock_size;

static struct vcan_sock *vcan_sock_find_l(int fd);

static _Thread_local struct vcan_sock *sock;

static unsigned int do_if_nametoindex(const char *ifname);
static char *do_if_indextoname(unsigned int ifindex, char *ifname);

static int do_bind_l(
		int sockfd, const struct sockaddr *addr, socklen_t addrlen);
static int do_getpeername_l(
		int sockfd, struct sockaddr *addr, socklen_t *addrlen);
static int do_getsockname_l(
		int sockfd, struct sockaddr *addr, socklen_t *addrlen);
static int do_getsockopt_l(int sockfd, int level, int optname, void *optval,
		socklen_t *optlen);
static ssize_t do_recv_l(int sockfd, void *buf, size_t len, int flags);
static ssize_t do_recvfrom_l(int sockfd, void *buf, size_t len, int flags,
		struct sockaddr *src_addr, socklen_t *addrlen);
static int do_recvmmsg_l(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
		int flags, struct timespec *timeout);
static ssize_t do_recvmsg_l(int sockfd, struct msghdr *msg, int flags);
static ssize_t do_send_l(int sockfd, const void *buf, size_t len, int flags);
static int do_sendmmsg_l(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
		int flags);
static ssize_t do_sendmsg_l(int sockfd, const struct msghdr *msg, int flags);
static ssize_t do_sendto_l(int sockfd, const void *buf, size_t len, int flags,
		const struct sockaddr *dest_addr, socklen_t addrlen);
static int do_setsockopt_l(int sockfd, int level, int optname,
		const void *optval, socklen_t optlen);
static int do_socket(int domain, int type, int protocol);

static ssize_t do_readv_l(int fd, const struct iovec *iov, int iovcnt);
static ssize_t do_writev_l(int fd, const struct iovec *iov, int iovcnt);

static int do_close_l(int fd);
static ssize_t do_read_l(int fd, void *buf, size_t count);
static ssize_t do_write_l(int fd, const void *buf, size_t count);

static int do_ioctl(int fd, unsigned long request, void *arg);

static inline int timespec_valid(const struct timespec *tp);
static struct timespec timespec_add(struct timespec lhs, struct timespec rhs);
static struct timespec timespec_sub(struct timespec lhs, struct timespec rhs);

static inline uint32_t ldn_u32(const unsigned char src[4]);
static inline void stn_u32(unsigned char dest[4], uint32_t x);

static size_t ld_iov(
		void *buf, size_t len, const struct iovec *iov, size_t iovlen);
static size_t st_iov(
		struct iovec *iov, size_t iovlen, const void *buf, size_t len);

static int ip_add_membership(
		int sockfd, const struct sockaddr *addr, socklen_t addrlen);

static int is_socket(int fd, int domain, int type, int protocol);

static inline int is_rtnetlink(int sockfd);

struct rtnl_answer {
	struct rtnl_answer *next;
	int sockfd;
	size_t len;
	struct nlmsghdr nlh;
};

static struct rtnl_answer *rtnl_answer_first = NULL;
static struct rtnl_answer **rtnl_answer_plast = &rtnl_answer_first;

static ssize_t rtnl_recvmsg_l(int sockfd, struct msghdr *msg, int flags);
static ssize_t rtnl_sendmsg_l(int sockfd, const struct msghdr *msg, int flags);

static int rtnl_close_l(int fd);

static int rtnl_is_getlink_vcan(const struct msghdr *msg, size_t count,
		unsigned int *pseq, const struct vcan_ifaddr **pifaddr);
static inline const struct rtattr *rta_find(const struct rtattr *rta,
		unsigned int len, unsigned short type);

unsigned int
if_nametoindex(const char *ifname)
{
	if (vcan_init() == -1)
		return 0;
	VCAN_TRACE_ALL();
	assert(next_if_nametoindex);

	unsigned int ifindex;
	if ((ifindex = do_if_nametoindex(ifname))) {
		VCAN_TRACE_CAN();
		return ifindex;
	}

	return next_if_nametoindex(ifname);
}

#if __GNUC__ >= 11
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-parameter"
#endif
char *
if_indextoname(unsigned int ifindex, char *ifname)
{
#if __GNUC__ >= 11
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return NULL;
	VCAN_TRACE_ALL();
	assert(next_if_indextoname);

	if (do_if_indextoname(ifindex, ifname)) {
		VCAN_TRACE_CAN();
		return ifname;
	}

	return next_if_indextoname(ifindex, ifname);
}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
int
bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_bind);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd))) {
		VCAN_TRACE_CAN();
		return do_bind_l(sockfd, addr, addrlen);
	}
	vcan_unlock();

	return next_bind(sockfd, addr, addrlen);
}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
int
getpeername(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_getpeername);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd))) {
		VCAN_TRACE_CAN();
		return do_getpeername_l(sockfd, addr, addrlen);
	}
	vcan_unlock();

	return next_getpeername(sockfd, addr, addrlen);
}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
int
getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_getsockname);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd))) {
		VCAN_TRACE_CAN();
		return do_getsockname_l(sockfd, addr, addrlen);
	}
	vcan_unlock();

	return next_getsockname(sockfd, addr, addrlen);
}

int
getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_getsockopt);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd))
			&& (level == SOL_SOCKET || level == SOL_CAN_RAW)) {
		VCAN_TRACE_CAN();
		return do_getsockopt_l(sockfd, level, optname, optval, optlen);
	}
	vcan_unlock();

	return next_getsockopt(sockfd, level, optname, optval, optlen);
}

ssize_t
recv(int sockfd, void *buf, size_t len, int flags)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_recv);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_recv_l(sockfd, buf, len, flags);
	}
	vcan_unlock();

	return next_recv(sockfd, buf, len, flags);
}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
ssize_t
recvfrom(int sockfd, void *buf, size_t len, int flags,
		struct sockaddr *src_addr, socklen_t *addrlen)
{
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_recvfrom);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_recvfrom_l(
				sockfd, buf, len, flags, src_addr, addrlen);
	}
	vcan_unlock();

	return next_recvfrom(sockfd, buf, len, flags, src_addr, addrlen);
}

int
recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen, int flags,
		struct timespec *timeout)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_recvmmsg);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_recvmmsg_l(sockfd, msgvec, vlen, flags, timeout);
	}
	vcan_unlock();

	return next_recvmmsg(sockfd, msgvec, vlen, flags, timeout);
}

ssize_t
recvmsg(int sockfd, struct msghdr *msg, int flags)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_recvmsg);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_recvmsg_l(sockfd, msg, flags);
	}
	vcan_unlock();

	return next_recvmsg(sockfd, msg, flags);
}

ssize_t
send(int sockfd, const void *buf, size_t len, int flags)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_send);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_send_l(sockfd, buf, len, flags);
	}
	vcan_unlock();

	return next_send(sockfd, buf, len, flags);
}

int
sendmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen, int flags)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_sendmmsg);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_sendmmsg_l(sockfd, msgvec, vlen, flags);
	}
	vcan_unlock();

	return next_sendmmsg(sockfd, msgvec, vlen, flags);
}

ssize_t
sendmsg(int sockfd, const struct msghdr *msg, int flags)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_sendmsg);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_sendmsg_l(sockfd, msg, flags);
	}
	vcan_unlock();

	return next_sendmsg(sockfd, msg, flags);
}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
ssize_t
sendto(int sockfd, const void *buf, size_t len, int flags,
		const struct sockaddr *dest_addr, socklen_t addrlen)
{
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_sendto);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) || is_rtnetlink(sockfd)) {
		VCAN_TRACE_CAN();
		return do_sendto_l(sockfd, buf, len, flags, dest_addr, addrlen);
	}
	vcan_unlock();

	return next_sendto(sockfd, buf, len, flags, dest_addr, addrlen);
}

int
setsockopt(int sockfd, int level, int optname, const void *optval,
		socklen_t optlen)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_setsockopt);

	vcan_lock();
	if ((sock = vcan_sock_find_l(sockfd)) && level == SOL_CAN_RAW) {
		VCAN_TRACE_CAN();
		return do_setsockopt_l(sockfd, level, optname, optval, optlen);
	}
	vcan_unlock();

	return next_setsockopt(sockfd, level, optname, optval, optlen);
}

int
socket(int domain, int type, int protocol)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_socket);

	if (domain == AF_CAN) {
		VCAN_TRACE_CAN();
		return do_socket(domain, type, protocol);
	}

	return next_socket(domain, type, protocol);
}

ssize_t
readv(int fd, const struct iovec *iov, int iovcnt)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_readv);

	vcan_lock();
	if ((sock = vcan_sock_find_l(fd)) || is_rtnetlink(fd)) {
		VCAN_TRACE_CAN();
		return do_readv_l(fd, iov, iovcnt);
	}
	vcan_unlock();

	return next_readv(fd, iov, iovcnt);
}

ssize_t
writev(int fd, const struct iovec *iov, int iovcnt)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_writev);

	vcan_lock();
	if ((sock = vcan_sock_find_l(fd)) || is_rtnetlink(fd)) {
		VCAN_TRACE_CAN();
		return do_writev_l(fd, iov, iovcnt);
	}
	vcan_unlock();

	return next_writev(fd, iov, iovcnt);
}

int
close(int fd)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_close);

	vcan_lock();
	if ((sock = vcan_sock_find_l(fd)) || is_rtnetlink(fd)) {
		VCAN_TRACE_CAN();
		return do_close_l(fd);
	}
	vcan_unlock();

	return next_close(fd);
}

ssize_t
read(int fd, void *buf, size_t count)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_read);

	vcan_lock();
	if ((sock = vcan_sock_find_l(fd)) || is_rtnetlink(fd)) {
		VCAN_TRACE_CAN();
		return do_read_l(fd, buf, count);
	}
	vcan_unlock();

	return next_read(fd, buf, count);
}

ssize_t
write(int fd, const void *buf, size_t count)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_write);

	vcan_lock();
	if ((sock = vcan_sock_find_l(fd)) || is_rtnetlink(fd)) {
		VCAN_TRACE_CAN();
		return do_write_l(fd, buf, count);
	}
	vcan_unlock();

	return next_write(fd, buf, count);
}

int
ioctl(int fd, unsigned long request, void *arg)
{
	if (vcan_init() == -1)
		return -1;
	VCAN_TRACE_ALL();
	assert(next_ioctl);

	if (!do_ioctl(fd, request, arg)) {
		VCAN_TRACE_CAN();
		return 0;
	}

	return next_ioctl(fd, request, arg);
}

static void
vcan_init_routine(void)
{
	const char *envval;

	vcan_hostid = gethostid();
	vcan_pid = getpid();

	if ((envval = getenv("VCAN_TRACE"))) {
		if (!strcasecmp(envval, "all")) {
			vcan_trace = TRACE_ALL;
		} else if (!strcasecmp(envval, "can")) {
			vcan_trace = TRACE_CAN;
		}
	}

	envval = getenv("VCAN_PORT");
	vcan_port = envval && *envval ? atoi(envval) : VCAN_PORT;
	envval = getenv("VCAN_IFINDEX");
	vcan_ifindex = envval && *envval ? atoi(envval) : VCAN_IFINDEX;

	envval = getenv("VCAN_IFADDR");
	if (!envval || !*envval)
		envval = VCAN_IFADDR;
	// Loop over the comma-separated list of interface definitions.
	while (*envval) {
		const char *begin = envval;
		const char *end = strchr(begin, ',');
		envval = end ? (end + 1) : (end = begin + strlen(begin));
		if (vcan_ifaddr_insert(begin, end) == -1) {
			vcan_init_errsv = errno;
			vcan_fini();
			return;
		}
	}

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif

	next_if_nametoindex = dlsym(RTLD_NEXT, "if_nametoindex");
	next_if_indextoname = dlsym(RTLD_NEXT, "if_indextoname");

	next_bind = dlsym(RTLD_NEXT, "bind");
	next_getpeername = dlsym(RTLD_NEXT, "getpeername");
	next_getsockname = dlsym(RTLD_NEXT, "getsockname");
	next_getsockopt = dlsym(RTLD_NEXT, "getsockopt");
	next_recv = dlsym(RTLD_NEXT, "recv");
	next_recvfrom = dlsym(RTLD_NEXT, "recvfrom");
	next_recvmmsg = dlsym(RTLD_NEXT, "recvmmsg");
	next_recvmsg = dlsym(RTLD_NEXT, "recvmsg");
	next_send = dlsym(RTLD_NEXT, "send");
	next_sendmmsg = dlsym(RTLD_NEXT, "sendmmsg");
	next_sendmsg = dlsym(RTLD_NEXT, "sendmsg");
	next_sendto = dlsym(RTLD_NEXT, "sendto");
	next_setsockopt = dlsym(RTLD_NEXT, "setsockopt");
	next_socket = dlsym(RTLD_NEXT, "socket");

	next_readv = dlsym(RTLD_NEXT, "readv");
	next_writev = dlsym(RTLD_NEXT, "writev");

	next_close = dlsym(RTLD_NEXT, "close");
	next_read = dlsym(RTLD_NEXT, "read");
	next_write = dlsym(RTLD_NEXT, "write");

	next_ioctl = dlsym(RTLD_NEXT, "ioctl");

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
}

static int
vcan_init(void)
{
	static pthread_once_t once_control = PTHREAD_ONCE_INIT;
	pthread_once(&once_control, &vcan_init_routine);
	if (vcan_init_errsv) {
		errno = vcan_init_errsv;
		return -1;
	}
	return 0;
}

static void
vcan_fini(void)
{
	while (rtnl_answer_first) {
		struct rtnl_answer *answer = rtnl_answer_first;
		rtnl_answer_first = rtnl_answer_first->next;
		free(answer);
	}
	rtnl_answer_plast = &rtnl_answer_first;

	free(vcan_ifaddr);
	vcan_ifaddr = NULL;
	vcan_ifaddr_size = 0;
}

static void
vcan_print_trace(const char *func)
{
	fflush(stdout);
	fprintf(stderr, "%s()\n", func);
}

static int
vcan_ifaddr_insert(const char *begin, const char *end)
{
	assert(begin);
	assert(end);
	assert(end >= begin);

	// Create a new entry.
	struct vcan_ifaddr *ifaddr = realloc(vcan_ifaddr,
			(vcan_ifaddr_size + 1) * sizeof(struct vcan_ifaddr));
	if (!ifaddr)
		return -1;
	vcan_ifaddr = ifaddr;
	ifaddr = &vcan_ifaddr[vcan_ifaddr_size];

	*ifaddr = (struct vcan_ifaddr)VCAN_IFADDR_INIT;
	ifaddr->if_index = vcan_ifindex + vcan_ifaddr_size;

	// Parse the interface name.
	const char *pos = strchr(begin, '=');
	if (!pos)
		return 0;
	memccpy(ifaddr->if_name, begin, 0, MIN(pos - begin, IF_NAMESIZE - 1));
	begin = pos + 1;

	// Parse the IP address.
	char buf[MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN)] = { 0 };
	memccpy(buf, begin, 0, MIN((size_t)(end - begin), sizeof(buf) - 1));
	// Convert the IP address.
	if (inet_pton(AF_INET6, buf, &ifaddr->sin6.sin6_addr) == 1) {
		ifaddr->sin6.sin6_family = AF_INET6;
		ifaddr->sin6.sin6_port = htons(vcan_port);
		ifaddr->addrlen = sizeof(ifaddr->sin6);
	} else if (inet_pton(AF_INET, buf, &ifaddr->sin.sin_addr) == 1) {
		ifaddr->sin.sin_family = AF_INET;
		ifaddr->sin.sin_port = htons(vcan_port);
		ifaddr->addrlen = sizeof(ifaddr->sin);
	} else {
		// Only accept IPv4 and IPv6 addresses.
		return 0;
	}

	vcan_ifaddr_size++;

	struct nlmsghdr *nlh = (struct nlmsghdr *)ifaddr->nlh;

	struct ifinfomsg *ifi = NLMSG_DATA(nlh);
	ifi->ifi_family = AF_UNSPEC;
	ifi->ifi_type = ARPHRD_CAN;
	ifi->ifi_index = ifaddr->if_index;
	ifi->ifi_flags = IFF_UP | IFF_RUNNING | IFF_NOARP | IFF_LOWER_UP
			| IFF_ECHO;

	struct rtattr *rta = IFLA_RTA(ifi);
	unsigned int len = (ifaddr->nlh + NLHMSG_BUFLEN) - (char *)rta;

	rta->rta_type = IFLA_IFNAME;
	rta->rta_len = RTA_LENGTH(strlen(ifaddr->if_name) + 1);
	strcpy(RTA_DATA(rta), ifaddr->if_name);

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_TXQLEN;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = 1000;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_OPERSTATE;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned char));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_LINKMODE;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned char));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_MTU;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = CANFD_MTU;

#ifdef IFLA_MIN_MTU
	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_MIN_MTU;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
#endif

#ifdef IFLA_MAX_MTU
	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_MAX_MTU;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
#endif

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_GROUP;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_PROMISCUITY;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_NUM_TX_QUEUES;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = 1;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_GSO_MAX_SEGS;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = 65536;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_GSO_MAX_SIZE;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = 65536;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_NUM_RX_QUEUES;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));
	*(unsigned int *)RTA_DATA(rta) = 1;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_CARRIER;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned char));
	*(unsigned char *)RTA_DATA(rta) = 1;

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_QDISC;
	rta->rta_len = RTA_LENGTH(strlen("noqueue") + 1);
	strcpy(RTA_DATA(rta), "noqueue");

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_CARRIER_CHANGES;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_CARRIER_UP_COUNT;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_CARRIER_DOWN_COUNT;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_PROTO_DOWN;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned int));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_MAP;
	rta->rta_len = RTA_LENGTH(sizeof(struct rtnl_link_ifmap));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_STATS64;
	rta->rta_len = RTA_LENGTH(sizeof(struct rtnl_link_stats64));

	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFLA_STATS;
	rta->rta_len = RTA_LENGTH(sizeof(struct rtnl_link_stats));

	struct rtattr *xdp = RTA_NEXT(rta, len);
	xdp->rta_type = IFLA_XDP;
	xdp->rta_len = 0;

	rta = RTA_DATA(xdp);
	rta->rta_type = IFLA_XDP_ATTACHED;
	rta->rta_len = RTA_LENGTH(sizeof(unsigned char));
	*(unsigned char *)RTA_DATA(rta) = XDP_ATTACHED_NONE;

	rta = RTA_NEXT(rta, len);
	xdp->rta_len = (char *)rta - (char *)xdp;

	struct rtattr *linkinfo = RTA_NEXT(rta, len);
	linkinfo->rta_type = IFLA_LINKINFO;
	linkinfo->rta_len = 0;

	rta = RTA_DATA(linkinfo);
	rta->rta_type = IFLA_INFO_KIND;
	rta->rta_len = RTA_LENGTH(strlen("vcan") + 1);
	strcpy(RTA_DATA(rta), "vcan");

	rta = RTA_NEXT(rta, len);
	linkinfo->rta_len = (char *)rta - (char *)linkinfo;

	nlh->nlmsg_len = NLMSG_ALIGN((char *)rta - (char *)nlh);

	return 0;
}

static const struct vcan_ifaddr *
vcan_ifaddr_find_by_index(unsigned int if_index)
{
	for (size_t i = 0; i < vcan_ifaddr_size; i++) {
		if (if_index == vcan_ifaddr[i].if_index)
			return &vcan_ifaddr[i];
	}
	return NULL;
}

static const struct vcan_ifaddr *
vcan_ifaddr_find_by_name(const char *if_name)
{
	for (size_t i = 0; i < vcan_ifaddr_size; i++) {
		if (!strncmp(if_name, vcan_ifaddr[i].if_name, IF_NAMESIZE))
			return &vcan_ifaddr[i];
	}
	return NULL;
}

static const struct vcan_ifaddr *
vcan_ifaddr_find_by_addr(const struct sockaddr *addr, socklen_t addrlen)
{
	assert(addr);

	switch (addr->sa_family) {
	case AF_INET:
		if (addrlen < sizeof(struct sockaddr_in))
			return NULL;
		const struct sockaddr_in *sin =
				(const struct sockaddr_in *)addr;
		for (size_t i = 0; i < vcan_ifaddr_size; i++) {
			if (sin->sin_family != vcan_ifaddr[i].sin.sin_family)
				continue;
			if (sin->sin_port != vcan_ifaddr[i].sin.sin_port)
				continue;
			// clang-format off
			if (memcmp(&sin->sin_addr, &vcan_ifaddr[i].sin.sin_addr,
					sizeof(struct in_addr)))
				// clang-format on
				continue;
			return &vcan_ifaddr[i];
		}
		return NULL;
	case AF_INET6:
		if (addrlen < sizeof(struct sockaddr_in6))
			return NULL;
		const struct sockaddr_in6 *sin6 =
				(const struct sockaddr_in6 *)addr;
		for (size_t i = 0; i < vcan_ifaddr_size; i++) {
			if (sin6->sin6_family
					!= vcan_ifaddr[i].sin6.sin6_family)
				continue;
			if (sin6->sin6_port != vcan_ifaddr[i].sin6.sin6_port)
				continue;
			// clang-format off
			if (memcmp(&sin6->sin6_addr,
					&vcan_ifaddr[i].sin6.sin6_addr,
					sizeof(struct in6_addr)))
				// clang-format on
				continue;
			return &vcan_ifaddr[i];
		}
		return NULL;
	case AF_CAN:
		if (addrlen < sizeof(struct sockaddr_can))
			return NULL;
		const struct sockaddr_can *can =
				(const struct sockaddr_can *)addr;
		return vcan_ifaddr_find_by_index(can->can_family);
	default: return NULL;
	}
}

static const struct vcan_ifaddr *
vcan_ifaddr_find_by_cmsg(const struct cmsghdr *cmsg)
{
	assert(cmsg);

	if (cmsg->cmsg_level == IPPROTO_IP && cmsg->cmsg_type == IP_PKTINFO) {
		const struct in_pktinfo *pktinfo =
				(const struct in_pktinfo *)CMSG_DATA(cmsg);
		struct sockaddr_in sin = { .sin_family = AF_INET,
			.sin_port = htons(vcan_port),
			.sin_addr = pktinfo->ipi_addr };
		return vcan_ifaddr_find_by_addr(
				(struct sockaddr *)&sin, sizeof(sin));
	} else if (cmsg->cmsg_level == IPPROTO_IPV6
			&& cmsg->cmsg_type == IPV6_PKTINFO) {
		const struct in6_pktinfo *pktinfo =
				(const struct in6_pktinfo *)CMSG_DATA(cmsg);
		struct sockaddr_in6 sin6 = { .sin6_family = AF_INET6,
			.sin6_port = htons(vcan_port),
			.sin6_addr = pktinfo->ipi6_addr };
		return vcan_ifaddr_find_by_addr(
				(struct sockaddr *)&sin6, sizeof(sin6));
	} else {
		return NULL;
	}
}

static inline void
vcan_lock(void)
{
	pthread_mutex_lock(&vcan_mutex);
}

static inline void
vcan_unlock(void)
{
	pthread_mutex_unlock(&vcan_mutex);
}

static struct vcan_sock *
vcan_sock_find_l(int fd)
{
	for (size_t i = 0; i < vcan_sock_size; i++) {
		if (vcan_sock[i].fd == fd)
			return &vcan_sock[i];
	}
	return NULL;
}

static unsigned int
do_if_nametoindex(const char *ifname)
{
	if (!ifname)
		return 0;

	for (size_t i = 0; i < vcan_ifaddr_size; i++) {
		const struct vcan_ifaddr *ifaddr =
				vcan_ifaddr_find_by_name(ifname);
		if (ifaddr)
			return ifaddr->if_index;
	}

	return 0;
}

static char *
do_if_indextoname(unsigned int ifindex, char *ifname)
{
	if (!ifname)
		return NULL;

	for (size_t i = 0; i < vcan_ifaddr_size; i++) {
		const struct vcan_ifaddr *ifaddr =
				vcan_ifaddr_find_by_index(ifindex);
		if (ifaddr) {
			memcpy(ifname, ifaddr->if_name, IF_NAMESIZE);
			return ifname;
		}
	}

	return NULL;
}

static int
do_bind_l(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
	assert(next_bind);
	assert(sock);
	assert(sock->fd == sockfd);

	if (sock->ifaddr) {
		vcan_unlock();
		// The socket is already bound to an address.
		errno = EINVAL;
		return -1;
	}

	if (!addr || addr->sa_family != AF_CAN
			|| addrlen < sizeof(struct sockaddr_can)) {
		vcan_unlock();
		// This is not a valid CAN address.
		errno = EINVAL;
		return -1;
	}

	unsigned int ifindex = ((const struct sockaddr_can *)addr)->can_ifindex;
	const struct vcan_ifaddr *ifaddr = NULL;
	if (ifindex != 0) {
		ifaddr = vcan_ifaddr_find_by_index(ifindex);
		if (!ifaddr) {
			vcan_unlock();
			// The virtual CAN network interface cannot be found.
			errno = EINVAL;
			return -1;
		}
	}
	sock->ifaddr = ifaddr;

	vcan_unlock();

	struct sockaddr_in6 sin6 = { .sin6_family = AF_INET6,
		.sin6_port = htons(vcan_port),
		.sin6_addr = in6addr_any };
	if (next_bind(sockfd, (struct sockaddr *)&sin6, sizeof(sin6)) == -1)
		return -1;

	if (ifaddr) {
		if (ip_add_membership(sockfd, &ifaddr->addr, ifaddr->addrlen)
				== -1)
			return -1;
	} else {
		for (size_t i = 0; i < vcan_ifaddr_size; i++) {
			// clang-format off
			if (ip_add_membership(sockfd, &vcan_ifaddr[i].addr,
					vcan_ifaddr[i].addrlen) == -1)
				// clang-format on
				return -1;
		}
	}

	return 0;
}

static int
do_getpeername_l(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
	assert(next_getpeername);
	assert(sock);
	assert(sock->fd == sockfd);
	(void)sockfd;
	(void)addr;
	(void)addrlen;

	vcan_unlock();
	errno = EOPNOTSUPP;
	return -1;
}

static int
do_getsockname_l(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
	assert(sock);
	assert(sock->fd == sockfd);
	(void)sockfd;

	const struct vcan_ifaddr *ifaddr = sock->ifaddr;
	vcan_unlock();

	if (!addr || !addrlen) {
		errno = EFAULT;
		return -1;
	}

	struct sockaddr_can can = { .can_family = AF_CAN,
		.can_ifindex = ifaddr ? ifaddr->if_index : 0 };
	memcpy(addr, &can, MIN(*addrlen, sizeof(can)));
	*addrlen = sizeof(can);

	return 0;
}

static int
do_getsockopt_l(int sockfd, int level, int optname, void *optval,
		socklen_t *optlen)
{
	assert(next_getsockopt);
	assert(sock);
	assert(sock->fd == sockfd);

	if (!optlen || (!optval && *optlen > 0)) {
		vcan_unlock();
		errno = EFAULT;
		return -1;
	}

	switch (level) {
	case SOL_SOCKET:
		vcan_unlock();
		switch (optname) {
		case SO_TYPE:
			memcpy(optval, &(int){ SOCK_RAW },
					MIN(sizeof(int), *optlen));
			*optlen = sizeof(int);
			return 0;
		case SO_PROTOCOL:
			memcpy(optval, &(int){ CAN_RAW },
					MIN(sizeof(int), *optlen));
			*optlen = sizeof(int);
			return 0;
		case SO_DOMAIN:
			memcpy(optval, &(int){ AF_CAN },
					MIN(sizeof(int), *optlen));
			*optlen = sizeof(int);
			return 0;
		default:
			return next_getsockopt(
					sockfd, level, optname, optval, optlen);
		}
	case SOL_CAN_RAW:
		switch (optname) {
		case CAN_RAW_FILTER: {
			socklen_t fsize =
					sock->count * sizeof(struct can_filter);
			memcpy(optval, sock->filter, MIN(fsize, *optlen));
			*optlen = fsize;
			vcan_unlock();
			return 0;
		}
		case CAN_RAW_ERR_FILTER:
			memcpy(optval, &sock->err_mask,
					MIN(sizeof(sock->err_mask), *optlen));
			*optlen = sizeof(sock->err_mask);
			vcan_unlock();
			return 0;
		case CAN_RAW_LOOPBACK:
			vcan_unlock();
			int loopback = 0;
			{
				char loop = 0;
				// clang-format off
				if (next_getsockopt(sockfd, IPPROTO_IP,
						IP_MULTICAST_LOOP, &loop,
						&(socklen_t){ sizeof(loop) })
						== -1)
					// clang-format on
					return -1;
				loopback = loopback && loop;
			}
			{
				int loop = 0;
				// clang-format off
				if (next_getsockopt(sockfd, IPPROTO_IPV6,
						IPV6_MULTICAST_LOOP, &loop,
						&(socklen_t){ sizeof(loop) })
						== -1)
					// clang-format on
					return -1;
				if (*optlen < sizeof(int)) {
					errno = EINVAL;
					return -1;
				}
				loopback = loopback && loop;
			}
			memcpy(optval, &loopback,
					MIN(sizeof(loopback), *optlen));
			*optlen = sizeof(loopback);
			return 0;
		case CAN_RAW_RECV_OWN_MSGS:
			memcpy(optval, &sock->recv_own_msgs,
					MIN(sizeof(sock->recv_own_msgs),
							*optlen));
			*optlen = sizeof(sock->recv_own_msgs);
			vcan_unlock();
			return 0;
		case CAN_RAW_FD_FRAMES:
			memcpy(optval, &sock->fd_frames,
					MIN(sizeof(sock->fd_frames), *optlen));
			*optlen = sizeof(sock->fd_frames);
			vcan_unlock();
			return 0;
		case CAN_RAW_JOIN_FILTERS:
			memcpy(optval, &sock->join_filters,
					MIN(sizeof(sock->join_filters),
							*optlen));
			*optlen = sizeof(sock->join_filters);
			vcan_unlock();
			return 0;
		default:
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
	default:
		vcan_unlock();
		return next_getsockopt(sockfd, level, optname, optval, optlen);
	}
}

static ssize_t
do_recv_l(int sockfd, void *buf, size_t len, int flags)
{
	return do_recvfrom_l(sockfd, buf, len, flags, NULL, NULL);
}

static ssize_t
do_recvfrom_l(int sockfd, void *buf, size_t len, int flags,
		struct sockaddr *src_addr, socklen_t *addrlen)
{
	struct iovec iov = { .iov_base = buf, .iov_len = len };
	struct msghdr msg = { .msg_name = src_addr,
		.msg_namelen = addrlen ? *addrlen : 0,
		.msg_iov = &iov,
		.msg_iovlen = 1 };
	ssize_t result = do_recvmsg_l(sockfd, &msg, flags);
	if (addrlen)
		*addrlen = msg.msg_namelen;
	return result;
}

static int
do_recvmmsg_l(int sockfd, struct mmsghdr *msgvec, unsigned int vlen, int flags,
		struct timespec *timeout)
{
	if (!msgvec && vlen) {
		vcan_unlock();
		errno = EFAULT;
		return -1;
	}
	vlen = MIN(vlen, UIO_MAXIOV);

	struct timespec end = { 0, 0 };
	if (timeout) {
		if (!timespec_valid(timeout)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		if (clock_gettime(CLOCK_MONOTONIC, &end) == -1) {
			vcan_unlock();
			return -1;
		}
		end = timespec_add(end, *timeout);
	}

	int errsv = errno;
	unsigned int datagrams = 0;
	while (datagrams < vlen) {
		ssize_t result = do_recvmsg_l(sockfd, &msgvec->msg_hdr,
				flags & ~MSG_WAITFORONE);
		vcan_lock();
		if (result < 0)
			break;
		msgvec->msg_len = result;

		datagrams++;

		if (msgvec->msg_hdr.msg_flags & MSG_OOB)
			break;
		msgvec++;

		if (flags & MSG_WAITFORONE)
			flags |= MSG_DONTWAIT;

		if (timeout) {
			struct timespec now = { 0, 0 };
			if (clock_gettime(CLOCK_MONOTONIC, &end) == -1)
				break;
			*timeout = timespec_sub(end, now);
			if (timeout->tv_sec < 0) {
				timeout->tv_sec = 0;
				timeout->tv_nsec = 0;
			}
			if (timeout->tv_sec == 0 && timeout->tv_nsec == 0)
				break;
		}
	}
	vcan_unlock();
	if (!datagrams && vlen)
		return -1;
	errno = errsv;
	return datagrams;
}

static ssize_t
do_recvmsg_l(int sockfd, struct msghdr *msg, int flags)
{
	assert(next_recvmsg);

	if (!sock) {
		return rtnl_recvmsg_l(sockfd, msg, flags);
	} else if (sock->fd != sockfd) {
		vcan_unlock();
		errno = EBADF;
		return -1;
	}

	vcan_unlock();

	if (!msg || (!msg->msg_name && msg->msg_namelen)
			|| (!msg->msg_iov && msg->msg_iovlen)
			|| (!msg->msg_control && msg->msg_controllen)) {
		errno = EFAULT;
		return -1;
	}

	for (size_t i = 0; i < msg->msg_iovlen; i++) {
		if (!msg->msg_iov[i].iov_base && msg->msg_iov[i].iov_len) {
			errno = EFAULT;
			return -1;
		}
	}

	unsigned char buf[VCANFD_MTU];
	struct iovec iov = { .iov_base = buf, .iov_len = sizeof(buf) };
	struct msghdr msg_ = { .msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_controllen = CMSG_SPACE_PKTINFO + msg->msg_controllen };
	msg_.msg_control = malloc(msg_.msg_controllen);
	if (!msg_.msg_control)
		return -1;

	ssize_t result = -1;
	int errsv = errno;
	for (;;) {
		errno = 0;
		msg_.msg_flags = msg->msg_flags;
		result = next_recvmsg(sockfd, &msg_, flags);
		if (result < 0) {
			errsv = errno;
			goto error;
		}
		// Ingnore invalid frame sizes.
		if (result >= 0 && result != VCAN_MTU && result != VCANFD_MTU)
			continue;
		// Check if the frame was sent from this host.
		if (ldn_u32(buf) == (uint32_t)vcan_hostid) {
			msg_.msg_flags |= MSG_DONTROUTE;
			// If the PID and file descriptor match, the frame was
			// sent by this socket.
			if (ldn_u32(buf + 4) == (uint32_t)vcan_pid
					&& ldn_u32(buf + 8) == (uint32_t)sockfd)
				msg_.msg_flags |= MSG_CONFIRM;
		}
		vcan_lock();
		sock = vcan_sock_find_l(sockfd);
		if (!sock) {
			vcan_unlock();
			errsv = EBADF;
			goto error;
		}
		if ((msg_.msg_flags & MSG_CONFIRM) && !sock->recv_own_msgs)
			goto reject;
		if (result == VCANFD_MTU && !sock->fd_frames)
			goto reject;
		canid_t can_id = ldn_u32(buf + 12);
		if (can_id & CAN_ERR_FLAG) {
			if (can_id & sock->err_mask)
				goto accept;
			goto reject;
		}
		const struct can_filter *filter = sock->filter;
		for (int i = 0; i < sock->count; i++, filter++) {
			int inv = (can_id & CAN_INV_FILTER) == CAN_INV_FILTER;
			can_id &= ~CAN_INV_FILTER;
			// Check if the CAN-ID matches the, possibly inverted,
			// filter.
			if (((can_id & filter->can_mask) == filter->can_id)
					^ inv) {
				if (!sock->join_filters)
					goto accept;
			} else if (sock->join_filters) {
				goto reject;
			}
		}
		// Always reject the frame if no filters are registered.
	reject:
		vcan_unlock();
		continue;

	accept:
		vcan_unlock();
		break;
	}
	assert(result >= 0);

	// Obtain and strip the multicast group address from the ancillary data
	// object information.
	struct sockaddr_can can = { .can_family = AF_CAN, .can_ifindex = 0 };
	struct cmsghdr *cmsg = CMSG_FIRSTHDR(msg);
	struct cmsghdr *cmsg_ = CMSG_FIRSTHDR(&msg_);
	for (; cmsg_; cmsg_ = CMSG_NXTHDR(&msg_, cmsg_)) {
		const struct vcan_ifaddr *ifaddr =
				vcan_ifaddr_find_by_cmsg(cmsg_);
		if (ifaddr) {
			can.can_ifindex = ifaddr->if_index;
		} else if (cmsg) {
			if (cmsg_->cmsg_len <= CMSG_MAXLEN(msg, cmsg)) {
				memcpy(cmsg, cmsg_, cmsg_->cmsg_len);
				cmsg = CMSG_NXTHDR(msg, cmsg);
			} else {
				cmsg = NULL;
			}
		}
	}
	if (msg->msg_name) {
		memcpy(msg->msg_name, &can, MIN(msg->msg_namelen, sizeof(can)));
		msg->msg_namelen = sizeof(can);
	}

	// Deserialize the CAN or CAN FD frame.
	if (result == VCAN_MTU) {
		result = CAN_MTU;
		struct can_frame frame = { .can_id = ldn_u32(buf + 12),
			.can_dlc = buf[16] };
		memcpy(frame.data, buf + 18, CAN_MAX_DLEN);
		st_iov(msg->msg_iov, msg->msg_iovlen, &frame, sizeof(frame));
	} else if (result == VCANFD_MTU) {
		result = CANFD_MTU;
		struct canfd_frame frame = { .can_id = ldn_u32(buf + 12),
			.len = buf[16],
			.flags = buf[17] };
		memcpy(frame.data, buf + 18, CANFD_MAX_DLEN);
		st_iov(msg->msg_iov, msg->msg_iovlen, &frame, sizeof(frame));
	}

	msg->msg_flags = msg_.msg_flags;

error:
	free(msg_.msg_control);
	errno = errsv;
	return result;
}

static ssize_t
do_send_l(int sockfd, const void *buf, size_t len, int flags)
{
	return do_sendto_l(sockfd, buf, len, flags, NULL, 0);
}

static int
do_sendmmsg_l(int sockfd, struct mmsghdr *msgvec, unsigned int vlen, int flags)
{
	if (!msgvec && vlen) {
		vcan_unlock();
		errno = EFAULT;
		return -1;
	}
	vlen = MIN(vlen, UIO_MAXIOV);

	int errsv = errno;
	unsigned int datagrams = 0;
	while (datagrams < vlen) {
		ssize_t result = do_sendmsg_l(sockfd, &msgvec->msg_hdr, flags);
		vcan_lock();
		if (result < 0)
			break;
		msgvec->msg_len = result;

		datagrams++;
		msgvec++;
	}
	vcan_unlock();
	if (!datagrams && vlen)
		return -1;
	errno = errsv;
	return datagrams;
}

static ssize_t
do_sendmsg_l(int sockfd, const struct msghdr *msg, int flags)
{
	assert(next_sendmsg);

	if (!sock) {
		return rtnl_sendmsg_l(sockfd, msg, flags);
	} else if (sock->fd != sockfd) {
		vcan_unlock();
		errno = EBADF;
		return -1;
	}

	const struct vcan_ifaddr *ifaddr = sock->ifaddr;
	int fd_frames = sock->fd_frames;
	vcan_unlock();

	if (!msg) {
		errno = EFAULT;
		return -1;
	}

	if (msg->msg_name)
		ifaddr = vcan_ifaddr_find_by_addr(
				msg->msg_name, msg->msg_namelen);
	if (!ifaddr) {
		errno = ENODEV;
		return -1;
	}

	if ((!msg->msg_name && msg->msg_namelen)
			|| (!msg->msg_iov && msg->msg_iovlen)
			|| (!msg->msg_control && msg->msg_controllen)) {
		errno = EFAULT;
		return -1;
	}

	size_t count = 0;
	for (size_t i = 0; i < msg->msg_iovlen; i++) {
		if (!msg->msg_iov[i].iov_base && msg->msg_iov[i].iov_len) {
			errno = EFAULT;
			return -1;
		}
		count += msg->msg_iov[i].iov_len;
	}

	unsigned char buf[VCANFD_MTU];
	size_t len = 0;
	// Store the host ID, PID and file descriptor, so the receiver can check
	// if the frame was sent by the same host and/or the same socket.
	stn_u32(buf + len, vcan_hostid);
	len += 4;
	stn_u32(buf + len, vcan_pid);
	len += 4;
	stn_u32(buf + len, sockfd);
	len += 4;
	// Serialize the CAN or CAN FD frame.
	if (count == CAN_MTU) {
		struct can_frame frame = { .can_id = 0 };
		ld_iov(&frame, sizeof(frame), msg->msg_iov, msg->msg_iovlen);
		stn_u32(buf + len, frame.can_id);
		len += 4;
		buf[len++] = frame.can_dlc;
		buf[len++] = 0;
		memcpy(buf + len, frame.data, CAN_MAX_DLEN);
		len += CAN_MAX_DLEN;
		assert(len == VCAN_MTU);
	} else if (fd_frames && count == CANFD_MTU) {
		struct canfd_frame frame = { .can_id = 0 };
		ld_iov(&frame, sizeof(frame), msg->msg_iov, msg->msg_iovlen);
		stn_u32(buf + len, frame.can_id);
		len += 4;
		buf[len++] = frame.len;
		buf[len++] = frame.flags;
		memcpy(buf + len, frame.data, CANFD_MAX_DLEN);
		len += CANFD_MAX_DLEN;
		assert(len == VCANFD_MTU);
	} else {
		errno = EINVAL;
		return -1;
	}

	struct iovec iov = { .iov_base = buf, .iov_len = len };
	struct msghdr msg_ = { .msg_name = (struct sockaddr *)&ifaddr->addr,
		.msg_namelen = ifaddr->addrlen,
		.msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_control = msg->msg_control,
		.msg_controllen = msg->msg_controllen };
	ssize_t result = next_sendmsg(sockfd, &msg_, flags);
	if (result == VCAN_MTU) {
		result = CAN_MTU;
	} else if (result == VCANFD_MTU) {
		result = CANFD_MTU;
	}
	return result;
}

static ssize_t
do_sendto_l(int sockfd, const void *buf, size_t len, int flags,
		const struct sockaddr *dest_addr, socklen_t addrlen)
{
	struct iovec iov = { .iov_base = (void *)buf, .iov_len = len };
	struct msghdr msg = { .msg_name = (struct sockaddr *)dest_addr,
		.msg_namelen = addrlen,
		.msg_iov = &iov,
		.msg_iovlen = 1 };
	return do_sendmsg_l(sockfd, &msg, flags);
}

static int
do_setsockopt_l(int sockfd, int level, int optname, const void *optval,
		socklen_t optlen)
{
	assert(next_setsockopt);
	assert(sock);
	assert(sock->fd == sockfd);
	assert(level == SOL_CAN_RAW);
	(void)level;

	if (!optval && optlen > 0) {
		vcan_unlock();
		errno = EFAULT;
		return -1;
	}

	switch (optname) {
	case CAN_RAW_FILTER:
		if (optlen % sizeof(struct can_filter)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		if (optlen > CAN_RAW_FILTER_MAX * sizeof(struct can_filter)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		int count = optlen / sizeof(struct can_filter);
		if (count > 1) {
			struct can_filter *filter = malloc(
					count * sizeof(struct can_filter));
			if (!filter) {
				vcan_unlock();
				errno = ENOMEM;
				return -1;
			}
			if (sock->filter != &sock->dfilter)
				free(sock->filter);
			sock->filter = filter;
		} else if (sock->filter != &sock->dfilter) {
			free(sock->filter);
			sock->filter = &sock->dfilter;
		}
		sock->count = count;
		memcpy(sock->filter, optval, optlen);
		for (int i = 0; i < sock->count; i++) {
			canid_t mask = sock->filter[i].can_mask;
			sock->filter[i].can_id &= (mask | CAN_INV_FILTER);
			sock->filter[i].can_mask &= ~CAN_INV_FILTER;
		}
		vcan_unlock();
		return 0;
	case CAN_RAW_ERR_FILTER:
		if (optlen != sizeof(sock->err_mask)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		memcpy(&sock->err_mask, optval, optlen);
		sock->err_mask &= CAN_ERR_MASK;
		vcan_unlock();
		return 0;
	case CAN_RAW_LOOPBACK: {
		vcan_unlock();
		if (optlen != sizeof(int)) {
			errno = EINVAL;
			return -1;
		}
		char loop = *(const int *)optval != 0;
		// clang-format off
		if (next_setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_LOOP,
				&loop, sizeof(loop)) == -1)
			// clang-format on
			return -1;
		// clang-format off
		if (next_setsockopt(sockfd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
				optval, optlen) == -1)
			// clang-format on
			return -1;
		return 0;
	}
	case CAN_RAW_RECV_OWN_MSGS:
		if (optlen != sizeof(sock->recv_own_msgs)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		memcpy(&sock->recv_own_msgs, optval, optlen);
		vcan_unlock();
		return 0;
	case CAN_RAW_FD_FRAMES:
		if (optlen != sizeof(sock->fd_frames)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		memcpy(&sock->fd_frames, optval, optlen);
		vcan_unlock();
		return 0;
	case CAN_RAW_JOIN_FILTERS:
		if (optlen != sizeof(sock->join_filters)) {
			vcan_unlock();
			errno = EINVAL;
			return -1;
		}
		memcpy(&sock->join_filters, optval, optlen);
		vcan_unlock();
		return 0;
	default:
		vcan_unlock();
		errno = EINVAL;
		return -1;
	}
}

static int
do_socket(int domain, int type, int protocol)
{
	assert(next_socket);
	assert(next_close);
	assert(next_setsockopt);
	assert(domain == AF_CAN);
	(void)domain;

	int errsv = 0;

	int flags = type & ~SOCK_TYPE_MASK;
	type &= SOCK_TYPE_MASK;

	if (flags & ~(SOCK_CLOEXEC | SOCK_NONBLOCK)) {
		errsv = EINVAL;
		goto error_arg;
	}

	if (type != SOCK_RAW) {
		errsv = EINVAL;
		goto error_arg;
	}

	if (protocol != CAN_RAW) {
		errsv = EPROTONOSUPPORT;
		goto error_arg;
	}

	int fd = next_socket(AF_INET6, SOCK_DGRAM | flags, IPPROTO_UDP);
	if (fd == -1) {
		errsv = errno;
		goto error_next_socket;
	}

	// clang-format off
	if (next_setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 },
			sizeof(int)) == -1) {
		// clang-format on
		errsv = errno;
		goto error_next_setsockopt;
	}

	// clang-format off
	if (next_setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY, &(int){ 0 },
			sizeof(int)) == -1) {
		// clang-format on
		errsv = errno;
		goto error_next_setsockopt;
	}

	// clang-format off
	if (next_setsockopt(fd, IPPROTO_IP, IP_PKTINFO, &(int){ 1 },
			sizeof(int)) == -1) {
		// clang-format on
		errsv = errno;
		goto error_next_setsockopt;
	}

	// clang-format off
	if (next_setsockopt(fd, IPPROTO_IPV6, IPV6_RECVPKTINFO, &(int){ 1 },
			sizeof(int)) == -1) {
		// clang-format on
		errsv = errno;
		goto error_next_setsockopt;
	}

	vcan_lock();

	sock = malloc((vcan_sock_size + 1) * sizeof(struct vcan_sock));
	if (!sock) {
		errsv = errno;
		goto error_malloc;
	}
	for (size_t i = 0; i < vcan_sock_size; i++) {
		sock[i] = vcan_sock[i];
		if (vcan_sock[i].filter == &vcan_sock[i].dfilter)
			sock[i].filter = &sock[i].dfilter;
	}
	free(vcan_sock);
	vcan_sock = sock;
	sock = &vcan_sock[vcan_sock_size++];

	*sock = (struct vcan_sock)VCAN_SOCK_INIT(sock);
	sock->fd = fd;

	assert(sock == vcan_sock_find_l(fd));

	vcan_unlock();

	return fd;

error_malloc:
	vcan_unlock();
error_next_setsockopt:
	next_close(fd);
error_next_socket:
error_arg:
	errno = errsv;
	return -1;
}

static ssize_t
do_readv_l(int fd, const struct iovec *iov, int iovcnt)
{
	struct msghdr msg = { .msg_iov = (struct iovec *)iov,
		.msg_iovlen = iovcnt };
	return do_recvmsg_l(fd, &msg, 0);
}

static ssize_t
do_writev_l(int fd, const struct iovec *iov, int iovcnt)
{
	struct msghdr msg = { .msg_iov = (struct iovec *)iov,
		.msg_iovlen = iovcnt };
	return do_sendmsg_l(fd, &msg, 0);
}

static int
do_close_l(int fd)
{
	assert(next_close);

	if (!sock)
		return rtnl_close_l(fd);

	assert(sock->fd == fd);
	assert(sock >= vcan_sock);
	assert(sock < vcan_sock + vcan_sock_size);

	if (sock->filter != &sock->dfilter)
		free(sock->filter);

	vcan_sock_size--;
	for (; sock < vcan_sock + vcan_sock_size; sock++) {
		sock[0] = sock[1];
		if (sock[1].filter == &sock[1].dfilter)
			sock[0].filter = &sock[0].dfilter;
	}

	sock = NULL;

	vcan_unlock();

	return next_close(fd);
}

static ssize_t
do_read_l(int fd, void *buf, size_t count)
{
	return do_recv_l(fd, buf, count, 0);
}

static ssize_t
do_write_l(int fd, const void *buf, size_t count)
{
	return do_send_l(fd, buf, count, 0);
}

static int
do_ioctl(int fd, unsigned long request, void *arg)
{
	(void)fd;

	if (!arg)
		return -1;

	struct ifreq *ifr = arg;
	const struct vcan_ifaddr *ifaddr;
	switch (request) {
	case SIOCGIFNAME:
		if ((ifaddr = vcan_ifaddr_find_by_index(ifr->ifr_ifindex))) {
			memcpy(ifr->ifr_name, ifaddr->if_name, IF_NAMESIZE);
			return 0;
		}
		break;
	case SIOCGIFINDEX:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_ifindex = ifaddr->if_index;
			return 0;
		}
		break;
	case SIOCGIFFLAGS:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_flags = IFF_UP | IFF_RUNNING | IFF_NOARP;
			return 0;
		}
		break;
	case SIOCGIFMTU:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_mtu = CANFD_MTU;
			return 0;
		}
		break;
	case SIOCGIFHWADDR:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_hwaddr = (struct sockaddr){
				.sa_family = ARPHRD_CAN
			};
			return 0;
		}
		break;
	case SIOCGIFMAP:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_map = (struct ifmap){ 0 };
			return 0;
		}
		break;
	case SIOCGIFTXQLEN:
		if ((ifaddr = vcan_ifaddr_find_by_name(ifr->ifr_name))) {
			ifr->ifr_qlen = 1000;
			return 0;
		}
	}

	return -1;
}

static inline int
timespec_valid(const struct timespec *tp)
{
	assert(tp);

	return tp->tv_sec >= 0 && tp->tv_nsec >= 0 && tp->tv_nsec < 1000000000l;
}

static struct timespec
timespec_add(struct timespec lhs, struct timespec rhs)
{
	assert(timespec_valid(&lhs));
	assert(timespec_valid(&rhs));

	struct timespec result = { .tv_sec = lhs.tv_sec + rhs.tv_sec,
		.tv_nsec = lhs.tv_nsec + rhs.tv_nsec };
	if (result.tv_nsec >= 1000000000l) {
		result.tv_sec++;
		result.tv_nsec -= 1000000000l;
	}
	if (result.tv_sec < lhs.tv_sec) {
		result.tv_sec = LONG_MAX;
		result.tv_nsec = 0;
	}
	return result;
}

static struct timespec
timespec_sub(struct timespec lhs, struct timespec rhs)
{
	assert(timespec_valid(&lhs));
	assert(timespec_valid(&rhs));

	struct timespec result = { .tv_sec = lhs.tv_sec - rhs.tv_sec,
		.tv_nsec = lhs.tv_nsec - rhs.tv_nsec };
	if (result.tv_nsec < 0) {
		result.tv_sec--;
		result.tv_nsec += 1000000000l;
	}
	if (result.tv_sec < lhs.tv_sec) {
		result.tv_sec = LONG_MAX;
		result.tv_nsec = 0;
	}
	return result;
}

static inline uint32_t
ldn_u32(const unsigned char src[4])
{
	uint32_t x = 0;
	memcpy(&x, src, 4);
	return ntohl(x);
}

static inline void
stn_u32(unsigned char dest[4], uint32_t x)
{
	x = htonl(x);
	memcpy(dest, &x, 4);
}

static size_t
ld_iov(void *buf, size_t len, const struct iovec *iov, size_t iovlen)
{
	assert(buf || !len);
	assert(iov || !iovlen);

	char *p = buf;
	size_t n = len;
	for (size_t i = 0; n && i < iovlen; i++) {
		if (!iov[i].iov_base || !iov[i].iov_len)
			continue;
		size_t iov_len = MIN(n, iov[i].iov_len);
		memcpy(p, iov[i].iov_base, iov_len);
		p += iov_len;
		n -= iov_len;
	}
	return len - n;
}

static size_t
st_iov(struct iovec *iov, size_t iovlen, const void *buf, size_t len)
{
	assert(buf || !len);
	assert(iov || !iovlen);

	const char *p = buf;
	size_t n = len;
	for (size_t i = 0; n && i < iovlen; i++) {
		if (!iov[i].iov_base || !iov[i].iov_len)
			continue;
		size_t iov_len = MIN(n, iov[i].iov_len);
		memcpy(iov[i].iov_base, p, iov_len);
		p += iov_len;
		n -= iov_len;
	}
	return len - n;
}

static int
ip_add_membership(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
	assert(next_setsockopt);
	assert(addr);
	assert(addrlen >= sizeof(struct sockaddr));

	if (addr->sa_family == AF_INET
			&& addrlen >= sizeof(struct sockaddr_in)) {
		const struct sockaddr_in *sin =
				(const struct sockaddr_in *)addr;
		struct ip_mreq mreq = { .imr_multiaddr = sin->sin_addr,
			.imr_interface = { INADDR_ANY } };
		return next_setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
				&mreq, sizeof(mreq));
	} else if (addr->sa_family == AF_INET6
			&& addrlen >= sizeof(struct sockaddr_in6)) {
		const struct sockaddr_in6 *sin6 =
				(const struct sockaddr_in6 *)addr;
		struct ipv6_mreq mreq = { .ipv6mr_multiaddr = sin6->sin6_addr,
			.ipv6mr_interface = 0 };
		return next_setsockopt(sockfd, IPPROTO_IPV6,
				IPV6_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
	} else {
		errno = EINVAL;
		return -1;
	}
}

static int
is_socket(int fd, int domain, int type, int protocol)
{
	assert(next_getsockopt);

	// Check if the file descriptor could be valid.
	if (fd < 0)
		return 0;
	// Check if the file descriptor is a socket.
	int errsv = errno;
	struct stat statbuf;
	if (fstat(fd, &statbuf) == -1) {
		errno = errsv;
		return 0;
	}
	if (!S_ISSOCK(statbuf.st_mode))
		return 0;
	if (domain != AF_UNSPEC) {
		int optval = AF_UNSPEC;
		// clang-format off
		if (next_getsockopt(fd, SOL_SOCKET, SO_DOMAIN, &optval,
				&(socklen_t){ sizeof(optval) }) == -1) {
			// clang-format on
			errno = errsv;
			return 0;
		}
		if (optval != domain)
			return 0;
	}
	if (type != 0) {
		int optval = 0;
		// clang-format off
		if (next_getsockopt(fd, SOL_SOCKET, SO_TYPE, &optval,
				&(socklen_t){ sizeof(optval) }) == -1) {
			// clang-format on
			errno = errsv;
			return 0;
		}
		if (optval != type)
			return 0;
	}
	if (protocol != 0) {
		int optval = 0;
		// clang-format off
		if (next_getsockopt(fd, SOL_SOCKET, SO_PROTOCOL, &optval,
				&(socklen_t){ sizeof(optval) }) == -1) {
			// clang-format on
			errno = errsv;
			return 0;
		}
		if (optval != protocol)
			return 0;
	}
	return 1;
}

static inline int
is_rtnetlink(int sockfd)
{
	return is_socket(sockfd, AF_NETLINK, 0, NETLINK_ROUTE);
}

static ssize_t
rtnl_recvmsg_l(int sockfd, struct msghdr *msg, int flags)
{
	assert(next_recvmsg);

	vcan_unlock();

	// First check if there are any pending responses from the kernel.
	int errsv = errno;
	ssize_t result = next_recvmsg(sockfd, msg, flags | MSG_DONTWAIT);
	if (result >= 0 || errno != EAGAIN)
		return result;
	errno = errsv;

	if (!msg || (!msg->msg_name && msg->msg_namelen)
			|| (!msg->msg_iov && msg->msg_iovlen)
			|| (!msg->msg_control && msg->msg_controllen)) {
		errno = EFAULT;
		return -1;
	}

	vcan_lock();
	struct rtnl_answer **panswer = &rtnl_answer_first;
	while (*panswer && (*panswer)->sockfd != sockfd)
		panswer = &(*panswer)->next;
	// If no stored response is found, fallback to the real recvmsg().
	if (!*panswer) {
		vcan_unlock();
		return next_recvmsg(sockfd, msg, flags);
	}
	struct rtnl_answer *answer = *panswer;
	ssize_t len = answer->len;
	result = st_iov(msg->msg_iov, msg->msg_iovlen, &answer->nlh, len);
	if (!(flags & MSG_PEEK)) {
		if (!(*panswer = (*panswer)->next))
			rtnl_answer_plast = panswer;
		free(answer);
	}
	vcan_unlock();

	if (msg->msg_name) {
		struct sockaddr_nl nl = { .nl_family = AF_NETLINK };
		memcpy(msg->msg_name, &nl, MIN(msg->msg_namelen, sizeof(nl)));
		msg->msg_namelen = sizeof(nl);
	}

	msg->msg_controllen = 0;

	msg->msg_flags = 0;
	if (result < len)
		msg->msg_flags |= MSG_TRUNC;

	return (flags & MSG_TRUNC) ? len : result;
}

static ssize_t
rtnl_sendmsg_l(int sockfd, const struct msghdr *msg, int flags)
{
	assert(next_sendmsg);
	assert(next_getsockname);

	vcan_unlock();

	if (!msg || (!msg->msg_iov && msg->msg_iovlen)) {
		errno = EFAULT;
		return -1;
	}

	size_t count = 0;
	for (size_t i = 0; i < msg->msg_iovlen; i++) {
		if (!msg->msg_iov[i].iov_base && msg->msg_iov[i].iov_len) {
			errno = EFAULT;
			return -1;
		}
		count += msg->msg_iov[i].iov_len;
	}

	int errsv = errno;

	// There is no point in intercepting rtnetlink requests if no virtual
	// CAN interfaces have been defined.
	if (!vcan_ifaddr_size)
		goto error;

	// Try to obtain the PID. If that causes an error, fallback to the real
	// sendmsg().
	// clang-format off
	struct sockaddr_nl nl = { .nl_family = AF_NETLINK };
	if (next_getsockname(sockfd, (struct sockaddr *)&nl,
			&(socklen_t){ sizeof(nl) }) == -1
			|| nl.nl_family != AF_NETLINK)
		// clang-format on
		goto error;

	// Check if this is an RTM_GETLINK request for one or more virtual CAN
	// interfaces. If not, fallback to the real sendmsg().
	unsigned int seq = 0;
	const struct vcan_ifaddr *ifaddr = NULL;
	int result = rtnl_is_getlink_vcan(msg, count, &seq, &ifaddr);
	if (result < 0)
		return -1;
	if (!result)
		goto error;

	// Allocate space for the RTM_NEWLINK response.
	size_t len = 0;
	if (ifaddr) {
		struct nlmsghdr *nlh = (struct nlmsghdr *)ifaddr->nlh;
		len += NLMSG_ALIGN(nlh->nlmsg_len);
	} else {
		for (size_t i = 0; i < vcan_ifaddr_size; i++) {
			struct nlmsghdr *nlh =
					(struct nlmsghdr *)vcan_ifaddr[i].nlh;
			len += NLMSG_ALIGN(nlh->nlmsg_len);
		}
		// Make room for the NLMSG_DONE message.
		len += NLMSG_SPACE(sizeof(int));
	}
	struct rtnl_answer *answer =
			malloc(offsetof(struct rtnl_answer, nlh) + len);
	if (!answer)
		goto error;

	answer->sockfd = sockfd;

	// Construct the RTM_NEWLINK response by copying the data from the
	// emulated interfaces.
	answer->len = len;
	struct nlmsghdr *nlh = &answer->nlh;
	if (ifaddr) {
		memcpy(nlh, ifaddr->nlh,
				((struct nlmsghdr *)ifaddr->nlh)->nlmsg_len);
		nlh->nlmsg_type = RTM_NEWLINK;
		nlh->nlmsg_flags = 0;
		nlh->nlmsg_seq = seq;
		nlh->nlmsg_pid = nl.nl_pid;
	} else {
		for (size_t i = 0; i < vcan_ifaddr_size;
				i++, nlh = NLMSG_NEXT(nlh, len)) {
			memcpy(nlh, vcan_ifaddr[i].nlh,
					((struct nlmsghdr *)vcan_ifaddr[i].nlh)
							->nlmsg_len);
			nlh->nlmsg_type = RTM_NEWLINK;
			nlh->nlmsg_flags = NLM_F_MULTI | NLM_F_DUMP_FILTERED;
			nlh->nlmsg_seq = seq;
			nlh->nlmsg_pid = nl.nl_pid;
		}
		// Append an NLMSG_DONE message to indicate that the response is
		// complete.
		nlh->nlmsg_len = NLMSG_LENGTH(sizeof(int));
		nlh->nlmsg_type = NLMSG_DONE;
		nlh->nlmsg_flags = NLM_F_MULTI;
		nlh->nlmsg_seq = seq;
		nlh->nlmsg_pid = nl.nl_pid;
		*(int *)NLMSG_DATA(nlh) = 0;
	}

	// Store the response.
	vcan_lock();
	*rtnl_answer_plast = answer;
	rtnl_answer_plast = &(*rtnl_answer_plast)->next;
	*rtnl_answer_plast = NULL;
	vcan_unlock();

	return count;

error:
	errno = errsv;
	return next_sendmsg(sockfd, msg, flags);
}

static int
rtnl_close_l(int fd)
{
	assert(next_close);

	// Delete any pending RTM_NEWLINK responses.
	for (struct rtnl_answer **panswer = &rtnl_answer_first; *panswer;
			panswer = &(*panswer)->next) {
		if ((*panswer)->sockfd != fd)
			continue;
		struct rtnl_answer *answer = *panswer;
		if (!(*panswer = (*panswer)->next))
			rtnl_answer_plast = panswer;
		free(answer);
	}

	vcan_unlock();

	return next_close(fd);
}

static int
rtnl_is_getlink_vcan(const struct msghdr *msg, size_t count, unsigned int *pseq,
		const struct vcan_ifaddr **pifaddr)
{
	assert(msg);
	assert(msg->msg_iov || !msg->msg_iovlen);

	int result = 0;
	int errsv = errno;
	unsigned int seq = 0;
	const struct vcan_ifaddr *ifaddr = NULL;

	void *buf = malloc(count);
	if (!buf)
		goto error_malloc;

	ld_iov(buf, count, msg->msg_iov, msg->msg_iovlen);
	const struct nlmsghdr *nlh = (struct nlmsghdr *)buf;
	// Only accept valid RTM_GETLINK requests.
	if (!NLMSG_OK(nlh, count))
		goto done;
	if (nlh->nlmsg_type != RTM_GETLINK)
		goto done;
	if ((nlh->nlmsg_flags & ~NLM_F_DUMP) != NLM_F_REQUEST)
		goto done;
	seq = nlh->nlmsg_seq;
	// Check if all CAN interfaces are requested.
	if (nlh->nlmsg_len == NLMSG_LENGTH(sizeof(struct rtgenmsg))) {
		result = ((struct rtgenmsg *)NLMSG_DATA(nlh))->rtgen_family
				== AF_CAN;
		goto done;
	}
	if (nlh->nlmsg_len < NLMSG_LENGTH(sizeof(struct ifinfomsg)))
		goto done;
	const struct ifinfomsg *ifi = NLMSG_DATA(nlh);
	if (ifi->ifi_type && ifi->ifi_type != ARPHRD_CAN)
		goto done;
	// Search by interface index.
	if (ifi->ifi_index) {
		ifaddr = vcan_ifaddr_find_by_index(ifi->ifi_index);
		result = ifaddr != NULL;
		goto done;
	}
	const struct rtattr *rta;
	// Search by interface name.
	if ((rta = rta_find(IFLA_RTA(ifi), IFLA_PAYLOAD(nlh), IFLA_IFNAME))) {
		ifaddr = vcan_ifaddr_find_by_name(RTA_DATA(rta));
		result = ifaddr != NULL;
		goto done;
	}
	// Check if all virtual CAN interfaces are requested.
	// clang-format off
	if ((rta = rta_find(IFLA_RTA(ifi), IFLA_PAYLOAD(nlh), IFLA_LINKINFO))
			&& (rta = rta_find(RTA_DATA(rta), RTA_PAYLOAD(rta),
					IFLA_INFO_KIND))
			&& !strcmp(RTA_DATA(rta), "vcan")) {
		// clang-format on
		result = 1;
		goto done;
	}

done:
	free(buf);
error_malloc:
	if (result > 0) {
		if (pseq)
			*pseq = seq;
		if (pifaddr)
			*pifaddr = ifaddr;
	}
	errno = errsv;
	return result;
}

static inline const struct rtattr *
rta_find(const struct rtattr *rta, unsigned int len, unsigned short type)
{
	assert(rta);

	for (; RTA_OK(rta, len); rta = RTA_NEXT(rta, len)) {
		if (rta->rta_type == type)
			return rta;
	}
	return NULL;
}
